package com.ticket.tk_cloud_gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TkCloudGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(TkCloudGatewayApplication.class, args);
    }

}
